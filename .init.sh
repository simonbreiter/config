#!/bin/sh

installHomebrewApps()
{
  echo "Installing homebrew..."
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

  echo "Installing homebrew applications..."

  brew install git \
  arping \
  imagemagick \
  nmap \
  pandoc \
  the_silver_searcher \
  tmux \
  tesseract \
  leptonica \
  cmake \
  potrace \
  ctags \
  ack \
  figlet \
  libtool \
  neovim \
  fish \
  reattach-to-user-namespace \
  curl \
  htop \
  jq \
  ledger \
  gdb \
  ngrep \
  doctl \
  mkcert \
  kubernetes-cli \
  kubectx \
  k9s \
  velero \
  helm \
  gcc \
  bpython \
  terraform \
  pstree \
  mongosh \
  volta \
  telegram \
  dropbox \
  iterm2 \
  appcleaner \
  firefox \
  arduino \
  transmission \
  vlc \
  sequel-pro \
  unity-hub \
  unity \
  alfred \
  blender \
  grandperspective \
  namechanger \
  typora \
  fork \
  metaz \
  mysqlworkbench \
  teamviewer \
  slack \
  wireshark \
  postman \
  docker \
  spotify \
  transmit \
  discord \
  pibakery \
  pgadmin4 \
  keybase \
  vcv-rack \
  jcryptool \
  steam \
  google-drive-file-stream \
  google-chrome \
  macpass \
  paw \
  burp-suite \
  whatsapp \
  autodesk-fusion360 \
  parallels

  echo "Configure apps..."
  terraform -install-autocomplete
}

installFishShell () 
{
  echo "Setting up fish shell..."
  sudo /bin/sh -c "echo "/usr/local/bin/fish" >> /etc/shells"
  chsh -s /usr/local/bin/fish
  cd /etc/ && sudo curl -s -O https://gitlab.com/simonbreiter/config/raw/master/motd.sh && cd
  sudo chmod +x /etc/motd.sh
  cd ~/.config/fish/ && curl -s -O https://gitlab.com/simonbreiter/config/raw/master/.config/fish/config.fish && cd
  echo "Install fisher..."
  curl https://git.io/fisher --create-dirs -sLo ~/.config/fish/functions/fisher.fish
  echo "Install fish plugins..."
  fisher install barnybug/docker-fish-completion
}

installRubyGems () 
{
  echo "Installing gems..."
  gem install --user-install bropages \
  cocoapods;
}

installNPMLibraries ()
{
  echo "Installing npm applications...";
  npm install -g license-generator \
  nodemon \
  node-inspector \
  neovim \
  http-server;
}

installVim ()
{
  echo "Setting up Nvim...";
  curl -s --create-dirs -o ~/.config/nvim/colors/Tomorrow-Night.vim https://gitlab.com/simonbreiter/config/raw/master/.vim/colors/Tomorrow-Night.vim;
  curl -s --create-dirs -o ~/.config/nvim/init.vim https://gitlab.com/simonbreiter/config/raw/master/.config/nvim/init.vim;

  echo "Installing vim plugins...";
  sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
        https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

  curl -s --create-dirs -o ~/.vimrc https://gitlab.com/simonbreiter/config/raw/master/.vimrc;
  curl -s --create-dirs -o ~/.ideavimrc https://gitlab.com/simonbreiter/config/raw/master/.ideavimrc;
}

installTmux ()
{

  echo "Setting up Tmux..."
  curl -s -o ~/.tmux.conf https://gitlab.com/simonbreiter/config/raw/master/.tmux.conf
  echo "Load new tmux config..."
  tmux source-file ~/.tmux.conf
}

installOSXConfiguration ()
{

      echo "Download custom Keyboard Layout"
      cd ~/Library/Keyboard\ Layouts && curl -s -O https://gitlab.com/simonbreiter/config/raw/master/Library/Keyboard%20Layouts/coding.keylayout && cd

      echo "Deactivate ApplePressAndHoldEnabled..."
      defaults write -g ApplePressAndHoldEnabled -bool false

      echo "Faster key repeat..."
      defaults write NSGlobalDomain KeyRepeat -int 1
      defaults write NSGlobalDomain InitialKeyRepeat -int 10

      echo "Configure git..."
      git config --global user.name "Simon Breiter"
      git config --global user.email hello@simonbreiter.com
      git config --global core.editor "nvim"
}


if [ "$EUID" -eq 0 ]
then 
    echo "Please do not run as root. Abort."
    exit
else 

  echo " _       _ _         _     "
  echo "(_)_ __ (_) |_   ___| |__  "
  echo "| | '_ \| | __| / __| '_ \ "
  echo "| | | | | | |_ _\__ \ | | |"
  echo "|_|_| |_|_|\__(_)___/_| |_|"
  echo "\nWelcome, ${USER}. What do you like to install?\n"
	PS3='Choose: '
	options=("All" "Homebrew Apps" "Fish Shell" "Ruby Gems" "NPM Libraries" "VIM" "Tmux" "OS X Configuration" "Quit")

	select opt in "${options[@]}"

	do

	  case $opt in

      "All")
	      installHomebrewApps
	      installFishShell
	      installRubyGems
	      installNPMLibraries
	      installVim
	      installTmux
	      installOSXConfiguration
        break
        ;;

	    "Homebrew Apps")
	      installHomebrewApps
	      break
	      ;;

	    "Fish Shell")
	      installFishShell
	      break
	      ;;

	    "Ruby Gems")
	      installRubyGems
	      break
	      ;;

	    "NPM Libraries")
	      installNPMLibraries
	      break
	      ;;

	    "VIM")
	      installVim
	      break
	      ;;

	    "Tmux")
	      installTmux
	      break
	      ;;

	    "OS X Configuration")
	      installOSXConfiguration
	      break
	      ;;

	    "Quit")
	      break
	      ;;

	     *) 
	      echo "Invalid option";;

	  esac

	done

fi
